![](https://elioway.gitlab.io/elioangels/apep/elio-apep-logo.png)

> The ancient Egyptian deity who embodied chaos **the elioWay**

# apep ![alpha](/artwork/icon/alpha/favicon.png "alpha")

A collection of util functions and methods writ in **python** **the elioWay**.

- [apep Documentation](https://elioway.gitlab.io/elioangels/apep)

## TODO

- Convert Schema.org to JSON (get "non-django" stuff from django app)
-
- Structure as a proper set if libraries specify for use in doing things the elioWay.

  ## Installing

```shell
pip install elio-apep
```

- [Installing apep](https://elioway.gitlab.io/elioangels/apep/installing.html)

## Seeing is Believing

```shell
git clone https://gitlab.com/elioangels/apep.git
virtualenv --python=python3 venv-apep
source venv-apep/bin/activate
# or
source venv-apep/bin/activate.fish
pip install -r requirements/local.txt
```

## Nutshell

### `py.test -x`

- [apep Quickstart](https://elioway.gitlab.io/elioangels/apep/quickstart.html)
- [apep Credits](https://elioway.gitlab.io/elioangels/apep/credits.html)

![](https://elioway.gitlab.io/elioangels/apep/apple-touch-icon.png)

## License

[MIT](LICENSE) [Tim Bushell](mailto:tcbushell@gmail.com)
