
def db_util_doc_as_dict(rows, columns):
    """Return select query as dict of rows and columns."""
    result_list = []
    for row in rows:
        row_def = {}
        x = 0
        for column in columns:
            row_def[column] = row[x]
            x += 1
        result_list.append(row_def)
    return result_list


def _db_connect():
    return sqlite3.connect(db_file_folder())


def db_execute(sql, params):
    """Execute sql query."""
    data = {}
    with _db_connect() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, params)
        conn.commit()
        cursor.close()
    data["status"] = "Completed"
    if hasattr(cursor, "lastrowid"):
        data["rowid"] = cursor.lastrowid
    return data


def _db_select(sql, params):
    """Select sql query."""
    conn = _db_connect()
    cursor = conn.cursor()
    cursor.execute(sql, params)
    columns = [column[0] for column in cursor.description]
    rows = cursor.fetchall()
    cursor.close()
    conn.close()
    return db_util_doc_as_dict(rows, columns)


def db_get_settings():
    rtn = _db_select(
        """
        SELECT
            SettingName,
            SettingValue
        FROM kbconnect_settings
        ORDER BY SettingName;
        """,
        {},
    )
    return rtn


def db_get_setting_value(setting_name):
    result = None
    rtn = _db_select(
        """
        SELECT
            SettingValue
        FROM kbconnect_settings
        WHERE
            SettingName=:setting_name;
        """,
        {"setting_name": setting_name},
    )
    if rtn:
        result = rtn[0]["SettingValue"]
    return result


def db_rem_setting_value(setting_name):
    return db_execute(
        """
        DELETE
        FROM kbconnect_settings
        WHERE
            SettingName=:setting_name;
        """,
        {"setting_name": setting_name},
    )


def db_set_setting_value(setting_name, setting_value):
    return db_execute(
        """
        INSERT OR REPLACE INTO kbconnect_settings(
            SettingName,
            SettingValue
        ) VALUES (
            :setting_name,
            :setting_value
        );
        """,
        {"setting_name": setting_name, "setting_value": setting_value},
    )


def db_local_log(log_type, method, command, version_pk, message, trace=None):
    return db_execute(
        """
        INSERT INTO kbconnect_log(
            Method,
            Command,
            VersionPK,
            Message,
            LogType,
            Created,
            Trace
        ) VALUES (
            :method,
            :command,
            :version_pk,
            :message,
            :log_type,
            :created,
            :trace
        );
        """,
        {
            "method": method,
            "command": command,
            "version_pk": version_pk,
            "message": message,
            "log_type": log_type,
            "created": datetime.now(),
            "trace": trace,
        },
    )


def db_log(log_type, method, command, version_pk, message, trace=None):
    """Log to both local database and the server.

        .. note:: do not call this from function that sends log to the server

    """
    ret = db_local_log(log_type, method, command, version_pk, message, trace)
    # reduce log traffic - don't send if log_type is "Watchdog' and 'Ping'
    if log_type not in ["Watchdog", "Ping"]:
        node_user_method = "[{}]\n{}\n{}".format(
            node_name(), get_user(), method
        )
        # import here to avoid cyclic import error
        from web import web_log_to_server

        web_log_to_server(
            ret["rowid"],
            log_type,
            node_user_method,
            command,
            version_pk,
            message,
            trace,
        )

    return ret


def db_get_logs():
    rtn = _db_select(
        "SELECT "
        + LOG_ROWS
        + """ FROM kbconnect_log
        WHERE LogType NOT IN ('Watchdog','Ping')
        ORDER BY Created DESC;
        """,
        {},
    )
    return rtn


def db_get_logtypes():
    rtn = _db_select(
        """
        SELECT
            LogType
        FROM kbconnect_log
        GROUP BY LogType
        ORDER BY LogType ASC;
        """,
        {},
    )
    return rtn


def db_get_logs_type(log_type):
    rtn = _db_select(
        "SELECT "
        + LOG_ROWS
        + """ FROM kbconnect_log
        WHERE LogType=:log_type
        ORDER BY Created DESC;
        """,
        {"log_type": log_type},
    )
    return rtn


def db_log_uploaded(rowid):
    rtn = db_execute(
        """UPDATE kbconnect_log
        SET Uploaded=1
        WHERE rowid=:rowid;
        """,
        {"rowid": rowid},
    )
    return rtn


def db_get_logs_version_pk(version_pk):
    rtn = _db_select(
        "SELECT "
        + LOG_ROWS
        + """ FROM kbconnect_log
        WHERE VersionPK=:version_pk
        ORDER BY Created DESC;
        """,
        {"version_pk": version_pk},
    )
    return rtn


def db_delete_logs_by_version_pk(version_pk):
    rtn = db_execute(
        """
        DELETE
        FROM kbconnect_log
        WHERE VersionPK=:version_pk;
        """,
        {"version_pk": version_pk},
    )
    return rtn


def db_delete_logs():
    rtn = db_execute(
        """
        DELETE
        FROM kbconnect_log;
        """,
        {},
    )
    return rtn


def db_create_tables():
    logger.info("db_create_tables")
    data = {}
    data["CREATE TABLE kbconnect_settings"] = db_execute(
        """
        CREATE TABLE IF NOT EXISTS kbconnect_settings(
            SettingName varchar(20) NOT NULL UNIQUE,
            SettingValue varchar(200) NOT NULL
            );
        """,
        {},
    )
    data["CREATE TABLE kbconnect_log"] = db_execute(
        """
        CREATE TABLE IF NOT EXISTS kbconnect_log(
            Method varchar(200) NOT NULL,
            Command varchar(200) NOT NULL,
            Message varchar(400) NOT NULL,
            VersionPK INTEGER NULL,
            LogType varchar(100) NOT NULL,
            Created DATETIME,
            Trace TEXT,
            Uploaded INTEGER NOT NULL DEFAULT 0
            );
        """,
        {},
    )
    return data


def db_drop_tables():
    data = {}
    data["DROP TABLE kbconnect_settings"] = db_execute(
        """
        DROP TABLE IF EXISTS kbconnect_settings;
        """,
        {},
    )
    data["DROP TABLE kbconnect_log"] = db_execute(
        """
        DROP TABLE IF EXISTS kbconnect_log;
        """,
        {},
    )
    return data
