# apep Credits

## Artwork

- [wikimedia:Apep](https://commons.wikimedia.org/wiki/File:Apep_1.jpg)

## These were useful

- <https://packaging.python.org/tutorials/packaging-projects/>
- <https://truveris.github.io/articles/configuring-pypirc/>
